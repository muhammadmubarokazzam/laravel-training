@extends('layout.master')

@section('judul')
<h1>Buat Account baru</h1>
@endsection

@section('content')
    <form action="/welcome" method="post">
    @csrf
        <h1>Buat Account Baru</h1>
        <p><b>Sign Up Form</b></p>

        <label>First name :</label><br><br>
        <input type="text" name="depan"><br><br>
        <label>Last name :</label><br><br>
        <input type="text" name="belakang"><br><br>

        <p>Gender :</p>
        <input type="radio" name="gender" value="0"> Male <br>
        <input type="radio" name="gender" value="1"> Female <br>
        <input type="radio" name="gender" value="2"> Other <br>

        <p>Nationality :</p>
        <select>
            <optgroup label="Asia">
                <option value="0">Indonesia</option>
                <option value="4">India</option>
                <option value="5">Malaysia</option>
                <option value="6">Japan</option>
            </optgroup>
            <optgroup label="Others">
                <option value="1">English</option>
                <option value="2">America</option>
                <option value="3">Russia</option>
            </optgroup>
        </select>

        <p>Language Spoken :</p>
        <input type="checkbox" name="Language" value="0"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language" value="1"> English <br>
        <input type="checkbox" name="Language" value="2"> Other <br>

        <p>Bio :</p>
        <textarea cols="30" rows="10"> </textarea>

        <br>
        <input type="submit" value="SignUp">
    
    </form>
@endsection