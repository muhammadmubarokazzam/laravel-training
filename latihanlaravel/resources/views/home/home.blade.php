@extends('layout.master')

@section('judul')
<h1>Media Online</h1>
@endsection

@section('content')
<div>
        
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <p><b>Benefit Join di Media Online</b></p>
    <ul>
        <li>mendapat motivasi dari sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
</div>
<div>
    <p><b>Cara Bergabung ke Media Online</b></p>
    <ol>
        <li>Mengunjungi Website ini </li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai </li>
    </ol>
</div>
@endsection